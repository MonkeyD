/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

/*  Monkey HTTP Daemon
 *  ------------------
 *  Copyright (C) 2001-2010, Eduardo Silva P. <edsiper@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "monkey.h"
#include "header.h"
#include "memory.h"
#include "request.h"
#include "iov.h"
#include "http_status.h"
#include "config.h"
#include "socket.h"
#include "utils.h"
#include "clock.h"
#include "cache.h"
#include "http.h"
#include "str.h"

int mk_header_iov_add_entry(struct mk_iov *mk_io, mk_pointer data,
                            mk_pointer sep, int free)
{
    return mk_iov_add_entry(mk_io, data.data, data.len, sep, free);
}

struct mk_iov *mk_header_iov_get()
{
    return mk_cache_get(mk_cache_iov_header);
}

void mk_header_iov_free(struct mk_iov *iov)
{
    mk_iov_free_marked(iov);
}

/* Send_Header , envia las cabeceras principales */
int mk_header_send(int fd, struct client_request *cr,
                   struct request *sr)
{
    int fd_status = 0;
    unsigned long len = 0;
    char *buffer = 0;
    struct header_values *sh;
    struct mk_iov *iov;

    sh = sr->headers;

    iov = mk_header_iov_get();

    /* Status Code */
    switch (sh->status) {
    case M_HTTP_OK:
        mk_header_iov_add_entry(iov, mk_hr_http_ok,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_HTTP_PARTIAL:
        mk_header_iov_add_entry(iov, mk_hr_http_partial,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_REDIR_MOVED:
        mk_header_iov_add_entry(iov, mk_hr_redir_moved,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_REDIR_MOVED_T:
        mk_header_iov_add_entry(iov, mk_hr_redir_moved_t,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_NOT_MODIFIED:
        mk_header_iov_add_entry(iov, mk_hr_not_modified,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_BAD_REQUEST:
        mk_header_iov_add_entry(iov, mk_hr_client_bad_request,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_FORBIDDEN:
        mk_header_iov_add_entry(iov, mk_hr_client_forbidden,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_NOT_FOUND:
        mk_header_iov_add_entry(iov, mk_hr_client_not_found,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_METHOD_NOT_ALLOWED:
        mk_header_iov_add_entry(iov, mk_hr_client_method_not_allowed,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_REQUEST_TIMEOUT:
        mk_header_iov_add_entry(iov, mk_hr_client_request_timeout,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_LENGTH_REQUIRED:
        mk_header_iov_add_entry(iov, mk_hr_client_length_required,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_CLIENT_REQUEST_ENTITY_TOO_LARGE:
        mk_header_iov_add_entry(iov, mk_hr_client_request_entity_too_large,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_SERVER_NOT_IMPLEMENTED:
        mk_header_iov_add_entry(iov, mk_hr_server_not_implemented,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_SERVER_INTERNAL_ERROR:
        mk_header_iov_add_entry(iov, mk_hr_server_internal_error,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;

    case M_SERVER_HTTP_VERSION_UNSUP:
        mk_header_iov_add_entry(iov,
                                mk_hr_server_http_version_unsup,
                                mk_iov_none, MK_IOV_NOT_FREE_BUF);
        break;
    default:
        return -1;
    };

    if (fd_status < 0) {
        mk_header_iov_free(iov);
        return -1;
    }

    /* Server details */
    mk_iov_add_entry(iov, sr->host_conf->header_host_signature.data,
                     sr->host_conf->header_host_signature.len,
                     mk_iov_crlf, MK_IOV_NOT_FREE_BUF);

    /* Date */
    mk_iov_add_entry(iov,
                     mk_header_short_date.data,
                     mk_header_short_date.len,
                     mk_iov_none, MK_IOV_NOT_FREE_BUF);
    mk_iov_add_entry(iov,
                     header_current_time.data,
                     header_current_time.len,
                     mk_iov_none, MK_IOV_NOT_FREE_BUF);

    /* Last-Modified */
    if (sh->last_modified > 0) {
        mk_pointer *lm;
        lm = mk_cache_get(mk_cache_header_lm);
        mk_utils_utime2gmt(&lm, sh->last_modified);

        mk_iov_add_entry(iov, mk_header_last_modified.data,
                         mk_header_last_modified.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
        mk_iov_add_entry(iov, lm->data, lm->len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
    }

    /* Connection */
    if (config->keep_alive == VAR_ON &&
        sr->keep_alive == VAR_ON &&
        cr->counter_connections < config->max_keep_alive_request) {

        /* A valid connection header */
        if (sr->connection.len > 0) {
            mk_string_build(&buffer,
                            &len,
                            "Keep-Alive: timeout=%i, max=%i"
                            MK_CRLF,
                            config->keep_alive_timeout,
                            (config->max_keep_alive_request -
                             cr->counter_connections)
                            );
            mk_iov_add_entry(iov, buffer, len, mk_iov_none, MK_IOV_FREE_BUF);
            mk_iov_add_entry(iov,
                             mk_header_conn_ka.data,
                             mk_header_conn_ka.len,
                             mk_iov_none, MK_IOV_NOT_FREE_BUF);
        }
    }
    else if(sr->close_now == VAR_ON) {
        mk_iov_add_entry(iov,
                         mk_header_conn_close.data,
                         mk_header_conn_close.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
    }

    /* Location */
    if (sh->location != NULL) {
        mk_iov_add_entry(iov,
                         mk_header_short_location.data,
                         mk_header_short_location.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);

        mk_iov_add_entry(iov,
                         sh->location,
                         strlen(sh->location), mk_iov_crlf, MK_IOV_FREE_BUF);
    }

    /* Content type */
    if (sh->content_type.len > 0) {
        mk_iov_add_entry(iov,
                         mk_header_short_ct.data,
                         mk_header_short_ct.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);

        mk_iov_add_entry(iov,
                         sh->content_type.data,
                         sh->content_type.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
    }

    /* Transfer Encoding: the transfer encoding header is just sent when
     * the response has some content defined by the HTTP status response 
     */
    if ((sh->status < M_REDIR_MULTIPLE) || (sh->status > M_REDIR_USE_PROXY)) {
        switch (sh->transfer_encoding) {
        case MK_HEADER_TE_TYPE_CHUNKED:
            mk_iov_add_entry(iov,
                             mk_header_te_chunked.data,
                             mk_header_te_chunked.len,
                             mk_iov_none, MK_IOV_NOT_FREE_BUF);
            break;
        }
    }

    /* Content-Length */
    if (sh->content_length >= 0 && sr->method != HTTP_METHOD_HEAD) {
        /* Map content length to MK_POINTER */
        mk_pointer *cl;
        cl = mk_cache_get(mk_cache_header_cl);
        mk_string_itop(sh->content_length, cl);
        
        /* Set headers */
        mk_iov_add_entry(iov, mk_header_content_length.data,
                         mk_header_content_length.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
        
        mk_iov_add_entry(iov, cl->data, cl->len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
        
    }

    if ((sh->content_length != 0 && 
         (sh->ranges[0] >= 0 || sh->ranges[1] >= 0)) &&
        config->resume == VAR_ON) {
        buffer = 0;

        /* yyy- */
        if (sh->ranges[0] >= 0 && sh->ranges[1] == -1) {
            mk_string_build(&buffer,
                            &len,
                            "%s bytes %d-%d/%d",
                            RH_CONTENT_RANGE,
                            sh->ranges[0],
                            (sh->real_length - 1), sh->real_length);
            mk_iov_add_entry(iov, buffer, len, mk_iov_crlf, MK_IOV_FREE_BUF);
        }

        /* yyy-xxx */
        if (sh->ranges[0] >= 0 && sh->ranges[1] >= 0) {
            mk_string_build(&buffer,
                            &len,
                            "%s bytes %d-%d/%d",
                            RH_CONTENT_RANGE,
                            sh->ranges[0], sh->ranges[1], sh->real_length);

            mk_iov_add_entry(iov, buffer, len, mk_iov_crlf, MK_IOV_FREE_BUF);
        }

        /* -xxx */
        if (sh->ranges[0] == -1 && sh->ranges[1] > 0) {
            mk_string_build(&buffer,
                            &len,
                            "%s bytes %d-%d/%d",
                            RH_CONTENT_RANGE,
                            (sh->real_length - sh->ranges[1]),
                            (sh->real_length - 1), sh->real_length);
            mk_iov_add_entry(iov, buffer, len, mk_iov_crlf, MK_IOV_FREE_BUF);
        }
    }
    
    if (sh->cgi == SH_NOCGI || sh->breakline == MK_HEADER_BREAKLINE) {
        mk_iov_add_entry(iov, mk_iov_crlf.data, mk_iov_crlf.len,
                         mk_iov_none, MK_IOV_NOT_FREE_BUF);
    }

    mk_socket_set_cork_flag(fd, TCP_CORK_ON);
    mk_iov_send(fd, iov, MK_IOV_SEND_TO_SOCKET);

#ifdef TRACE
    MK_TRACE("Headers sent to FD %i", fd);
    printf("%s", ANSI_YELLOW);
    fflush(stdout);
    mk_iov_send(0, iov, MK_IOV_SEND_TO_SOCKET);
    printf("%s", ANSI_RESET);
    fflush(stdout);
#endif

    mk_header_iov_free(iov);
    return 0;
}

char *mk_header_chunked_line(int len)
{
    char *buf;

    buf = mk_mem_malloc_z(10);
    snprintf(buf, 9, "%x%s", len, MK_CRLF);

    return buf;
}

void mk_header_set_http_status(struct request *sr, int status)
{
    sr->headers->status = status;
}

struct header_values *mk_header_create()
{
    struct header_values *headers;

    headers =
        (struct header_values *) mk_mem_malloc(sizeof(struct header_values));
    headers->status = 0;
    headers->ranges[0] = -1;
    headers->ranges[1] = -1;
    headers->content_length = -1;
    headers->transfer_encoding = -1;
    headers->last_modified = -1;
    mk_pointer_reset(&headers->content_type);
    headers->location = NULL;

    return headers;
}
